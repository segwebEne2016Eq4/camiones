package controller;

import entity.Camiones;
import controller.util.JsfUtil;
import controller.util.JsfUtil.PersistAction;
import session.CamionesFacade;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("camionesController")
@SessionScoped
public class CamionesController implements Serializable {

    @EJB
    private session.CamionesFacade ejbFacade;
    private List<Camiones> items = null;
    private Camiones selected;

    public CamionesController() {
    }

    public Camiones getSelected() {
        return selected;
    }

    public void setSelected(Camiones selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private CamionesFacade getFacade() {
        return ejbFacade;
    }

    public Camiones prepareCreate() {
        selected = new Camiones();
        initializeEmbeddableKey();
        return selected;
    }

    public void editView() {
        if (selected == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Advertencia!", "Seleccione un camión de la lista"));
        } else {
            Map<String, Object> options = new HashMap<String, Object>();
            options.put("modal", true);
            options.put("width", 640);
            options.put("height", 340);
            options.put("contentWidth", "100%");
            options.put("contentHeight", "100%");
            RequestContext.getCurrentInstance().openDialog("editCamiones", options, null);
        }
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("CamionesCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("CamionesUpdated"));
    }

    public void destroy() {
        if (selected == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Advertencia!", "Seleccione un camión de la lista"));
        } else {
            persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("CamionesDeleted"));
            if (!JsfUtil.isValidationFailed()) {
                selected = null; // Remove selection
                items = null;    // Invalidate list of items to trigger re-query.
            }
        }
    }

    public List<Camiones> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Camiones getCamiones(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Camiones> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Camiones> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    public void delete(Camiones cam) {
        getFacade().remove(cam);
    }

    public String tipoCamion(int tipo) {
        switch (tipo) {
            case 1:
                return "Un piso";
            case 2:
                return "Dos pisos";
            default:
                return "No registrado";
        }
    }

    public String statusCamion(int tipo) {
        switch (tipo) {
            case 1:
                return "Disponible";
            case 2:
                return "En Reparación";
            case 3:
                return "En Viaje";
            default:
                return "Desconocido";
        }
    }

    @FacesConverter(forClass = Camiones.class)
    public static class CamionesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CamionesController controller = (CamionesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "camionesController");
            return controller.getCamiones(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Camiones) {
                Camiones o = (Camiones) object;
                return getStringKey(o.getIdCamion());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Camiones.class.getName()});
                return null;
            }
        }

    }

}
