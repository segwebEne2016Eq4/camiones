/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import control.exceptions.RollbackFailureException;
import entity.Tarjetas;
import entity.Usuarios;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import utils.encriptador;

/**
 *
 * @author Chris
 */
@Named(value = "altaBean")
@SessionScoped
public class AltaBean implements Serializable {

    //private static final long serialVersionUID = 1L;
    private String username;
    private String nombre;
    private String apPat;
    private String apMat;
    private String correo;
    private String contrasenaOld;
    private String contrasenaEnc;
    private String contrasena;
    private String confirmpwd;
    private String telefono;

    private String no_tarjeta;
    private Date fecha_cad;
    private String codigo_seg;

    @Inject
    UsuariosJpaController controlUsers;
    @Inject
    TarjetasJpaController controlTarjetas;

    Usuarios user;
    Tarjetas card;
    Locale locale = new Locale("es");
    SimpleDateFormat formateador = new SimpleDateFormat("EEE dd MMM yyyy", locale);

    encriptador e = new encriptador();

    /**
     * Creates a new instance of AltaBean
     */
    public AltaBean() {
    }

    public String alta() throws RollbackFailureException, Exception {
        String ret;
        Tarjetas perCard, tempCard;

        card = new Tarjetas();
        card.setNoTarjeta(no_tarjeta);
        card.setCodigoSeg(Integer.parseInt(codigo_seg));
        card.setFechaCad(fecha_cad);

        user = new Usuarios();
        user.setTelefono(telefono);
        user.setNombre(nombre);
        user.setApPat(apPat);
        user.setApMat(apMat);
        user.setCorreo(correo);
        //user.setContrasena(contrasena);
        user.setUsername(username);

        if (contrasena.equals(confirmpwd)) {
            user.setContrasena(e.encripta(contrasena));

            tempCard = controlTarjetas.create(card);
            System.out.println("Card retrieved ");
            if (tempCard != null) {
                System.out.println("Card Persisted: " + tempCard.getNoTarjeta());
                Usuarios tempUser, userMail;

                user.setIdTarjeta(tempCard);
                System.out.println("querying card");
                tempUser = controlUsers.findUsuarios(username);
                userMail = controlUsers.findByMail(correo);
                if (tempUser == null && userMail == null) {
                    controlUsers.create(user);
                    System.out.println("User persisted: " + user.getUsername());

                    //externalContext.redirect(externalContext.getRequestContextPath() + "/index.xhtml");
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos creados correctamente", "Info"));
                    ret = "index?faces-redirect=true";
                } else {
                    controlTarjetas.destroy(tempCard.getIdTarjeta());
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El ususario/correo ya esta registrado", "Error"));
                    ret = "";
                }
            } else {
                System.out.println("unable to create card");
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "La tarjeta ya esta en uso", "Error"));
                ret = "index";
            }
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las contraseñas no coinciden", "Error"));
            ret = "";
        }

        return ret;
    }

    public void consulta() {
        user = controlUsers.findUsuarios(username);

        if (user == null) {
            //username = null;
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario no esta registrado", "Error"));
        } else {

            username = user.getUsername();
            telefono = user.getTelefono();
            nombre = user.getNombre();
            apPat = user.getApPat();
            apMat = user.getApMat();
            correo = user.getCorreo();
            contrasenaEnc = user.getContrasena();

            card = user.getIdTarjeta();
            no_tarjeta = card.getNoTarjeta();
            codigo_seg = (card.getCodigoSeg() == null) ? "--" : card.getCodigoSeg().toString();
            fecha_cad = card.getFechaCad();

            System.out.println("User retrieved: " + user.getUsername() + "-" + user.getCorreo()
                    + "\n Tarjeta: " + card.getNoTarjeta() + "-" + card.getIdTarjeta());
            //user.setContrasena(contrasena);
        }
    }

    public String update() throws RollbackFailureException, Exception {
        String ret;

        System.out.println("retrieving data for user: " + user.getUsername());
        //user = controlUsers.findUsuarios(username);
        //card = user.getIdTarjeta();

        System.out.println("trying to update user: " + user.getUsername()
                + "\n     With card: " + card.getNoTarjeta() + "-" + card.getIdTarjeta());

        System.out.println("\nConstrasenas:-" + contrasenaEnc
                + "\n             -" + e.encripta(contrasenaOld));
        if (contrasenaEnc.equals(e.encripta(contrasenaOld)) && contrasena.equals(confirmpwd)) {
            if (card != null) {
                user = new Usuarios();
                user.setUsername(username);
                user.setNombre(nombre);
                user.setApPat(apPat);
                user.setApMat(apMat);
                user.setContrasena(e.encripta(contrasena));
                user.setTelefono(telefono);
                user.setCorreo(correo);
                user.setIdTarjeta(card);

                card.setCodigoSeg(Integer.parseInt(codigo_seg));
                card.setFechaCad(fecha_cad);
                card.setNoTarjeta(no_tarjeta);

                controlTarjetas.edit(card);
                controlUsers.edit(user);

                logout();
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos actualizados", "Info"));
                ret = "login?faces-redirect=true";
            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Por favor, verifique los datos", "Error"));
                ret = "";
            }
        } else {
            //logout();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Porfavor verifique la contraseña anterior, asi como que la nueva contraseña sea confirmada correctamente", "Info"));
            ret = "";
        }
        return ret;
    }

    public void cancelEdit() throws IOException {
        this.username = null;
        this.nombre = null;
        this.apPat = null;
        this.apMat = null;
        this.correo = null;
        this.contrasenaOld = null;
        this.contrasenaEnc = null;
        this.contrasena = null;
        this.confirmpwd = null;
        this.telefono = null;

        this.no_tarjeta = null;
        this.fecha_cad = null;
        this.codigo_seg = null;
        
        logout();
    }

    public void logout() throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.invalidateSession();
        //externalContext.redirect(externalContext.getRequestContextPath() + "/facs/index.xhtml");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApPat() {
        return apPat;
    }

    public void setApPat(String apPat) {
        this.apPat = apPat;
    }

    public String getApMat() {
        return apMat;
    }

    public void setApMat(String apMat) {
        this.apMat = apMat;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNo_tarjeta() {
        return no_tarjeta;
    }

    public void setNo_tarjeta(String no_tarjeta) {
        this.no_tarjeta = no_tarjeta;
    }

    public Date getFecha_cad() {
        return fecha_cad;
    }

    public void setFecha_cad(Date fecha_cad) {
        this.fecha_cad = fecha_cad;
    }

    public String getCodigo_seg() {
        return codigo_seg;
    }

    public void setCodigo_seg(String codigo_seg) {
        this.codigo_seg = codigo_seg;
    }

    public String getConfirmpwd() {
        return confirmpwd;
    }

    public void setConfirmpwd(String confirmpwd) {
        this.confirmpwd = confirmpwd;
    }

    public Usuarios getUser() {
        return user;
    }

    public String getContrasenaOld() {
        return contrasenaOld;
    }

    public void setContrasenaOld(String contrasenaOld) {
        this.contrasenaOld = contrasenaOld;
    }

    public void setUser(Usuarios user) {
        this.user = user;
    }

    public Tarjetas getCard() {
        return card;
    }

    public void setCard(Tarjetas card) {
        this.card = card;
    }

}
