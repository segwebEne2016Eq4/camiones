/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Usuarios;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Chris
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private String username;
    private String contrasena;

    @Inject
    UsuariosJpaController controlUsers;
    Usuarios user;
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }
    
    @PostConstruct
    public void init(){
        user= null;
    }

    public String login() throws IOException {
        String pagina = null;

        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        try {
            System.out.println("Attempting to login " + username + "-" + contrasena);
            request.login(username, contrasena);
            System.out.println("Role retrieved");

            if (request.isUserInRole("admin")) {
                user = controlUsers.findUsuarios(username);
                pagina = "/admin/adminIdx.xhtml?faces-redirect=true";
            } else if (request.isUserInRole("users")) {
                user = controlUsers.findUsuarios(username);
                pagina = "/users/userIdx.xhtml?faces-redirect=true";
            } else if (request.isUserInRole("viajes")) {
                user = controlUsers.findUsuarios(username);
                pagina = "/admin/viajes/index.xhtml?faces-redirect=true";
            } else if (request.isUserInRole("destinos")) {
                user = controlUsers.findUsuarios(username);
                pagina = "/admin/destinos/index.xhtml?faces-redirect=true";
            } else {
                user = null;
                System.out.println("No role");
                context.addMessage(null, new FacesMessage("Credenciales invalidas"));
                externalContext = FacesContext.getCurrentInstance().getExternalContext();
                externalContext.invalidateSession();
                pagina = "login";

            }
            System.out.println("page retrieved: " + pagina);
        } catch (ServletException ex) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error de convinación usuario/contraseña", "Info"));
            System.out.println("Couldn't login");
            //ex.printStackTrace();
            pagina = "login";
        }

        return pagina;
    }

    public void logout() throws IOException {
        System.out.println("Logging out");
        user = null;
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.invalidateSession();
        externalContext.redirect(externalContext.getRequestContextPath() + "/faces/login.xhtml?faces-redirect=true");
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Usuarios getUser() {
        return user;
    }

    public void setUser(Usuarios user) {
        this.user = user;
    }

    
}
