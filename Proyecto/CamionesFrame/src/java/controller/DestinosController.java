package controller;

import entity.Destinos;
import controller.util.JsfUtil;
import controller.util.JsfUtil.PersistAction;
import session.DestinosFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import session.AbstractFacade;

@Named("destinosController")
@SessionScoped
public class DestinosController implements Serializable {
    private String destino;
    private String tiempoRecorrido;
    private float tarifa;
    private List<String> LDestinos = new ArrayList<>();
    public String getDestino() {
        return destino;
    }

    public List<String> getLDestinos() {
        return LDestinos;
    }

    public void setLDestinos(List<String> LDestinos) {
        this.LDestinos = LDestinos;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getTiempoRecorrido() {
        return tiempoRecorrido;
    }

    public void setTiempoRecorrido(String tiempoRecorrido) {
        this.tiempoRecorrido = tiempoRecorrido;
    }

    public float getTarifa() {
        return tarifa;
    }

    public void setTarifa(float tarifa) {
        this.tarifa = tarifa;
    }
    @EJB
    private session.DestinosFacade ejbFacade;
    private AbstractFacade FiFD;
    private List<Destinos> items = null;
    private Destinos selected;

    public List<Destinos> cargarDestinos(){
        return this.FiFD.findAll();
    }
    public DestinosController() {
    }

    public Destinos getSelected() {
        return selected;
    }

    public void setSelected(Destinos selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private DestinosFacade getFacade() {
        return ejbFacade;
    }

    public Destinos prepareCreate() {
        selected = new Destinos();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("DestinosCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("DestinosUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("DestinosDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Destinos> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Destinos getDestinos(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Destinos> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Destinos> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Destinos.class)
    public static class DestinosControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            DestinosController controller = (DestinosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "destinosController");
            return controller.getDestinos(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Destinos) {
                Destinos o = (Destinos) object;
                return getStringKey(o.getIdDestino());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Destinos.class.getName()});
                return null;
            }
        }

    }

}
