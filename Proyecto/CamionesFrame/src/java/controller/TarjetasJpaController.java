package controller;

import control.exceptions.NonexistentEntityException;
import control.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entity.Usuarios;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import entity.Tarjetas;

/**
 *
 * @author krozz
 */
@Named
@RequestScoped
public class TarjetasJpaController implements Serializable {

    @Resource
    UserTransaction utx;
    @PersistenceContext(unitName="CamionesFramePU")
    EntityManager em;
    
    public TarjetasJpaController(){
        
    }


    public Tarjetas create(Tarjetas tarjetas) throws RollbackFailureException, Exception {
        if (tarjetas.getUsuariosList() == null) {
            tarjetas.setUsuariosList(new ArrayList<Usuarios>());
        }
        try {
            utx.begin();
            List<Usuarios> attachedUsuariosList = new ArrayList<Usuarios>();
            for (Usuarios usuariosListUsuariosToAttach : tarjetas.getUsuariosList()) {
                usuariosListUsuariosToAttach = em.getReference(usuariosListUsuariosToAttach.getClass(), usuariosListUsuariosToAttach.getUsername());
                attachedUsuariosList.add(usuariosListUsuariosToAttach);
            }
            tarjetas.setUsuariosList(attachedUsuariosList);
            em.persist(tarjetas);
            for (Usuarios usuariosListUsuarios : tarjetas.getUsuariosList()) {
                Tarjetas oldIdTarjetaOfUsuariosListUsuarios = usuariosListUsuarios.getIdTarjeta();
                usuariosListUsuarios.setIdTarjeta(tarjetas);
                usuariosListUsuarios = em.merge(usuariosListUsuarios);
                if (oldIdTarjetaOfUsuariosListUsuarios != null) {
                    oldIdTarjetaOfUsuariosListUsuarios.getUsuariosList().remove(usuariosListUsuarios);
                    oldIdTarjetaOfUsuariosListUsuarios = em.merge(oldIdTarjetaOfUsuariosListUsuarios);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                tarjetas = null;
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } 
        return tarjetas;
    }

    public void edit(Tarjetas tarjetas) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Tarjetas persistentTarjetas = em.find(Tarjetas.class, tarjetas.getIdTarjeta());
            List<Usuarios> usuariosListOld = persistentTarjetas.getUsuariosList();
            List<Usuarios> usuariosListNew = tarjetas.getUsuariosList();
            List<Usuarios> attachedUsuariosListNew = new ArrayList<Usuarios>();
            for (Usuarios usuariosListNewUsuariosToAttach : usuariosListNew) {
                usuariosListNewUsuariosToAttach = em.getReference(usuariosListNewUsuariosToAttach.getClass(), usuariosListNewUsuariosToAttach.getUsername());
                attachedUsuariosListNew.add(usuariosListNewUsuariosToAttach);
            }
            usuariosListNew = attachedUsuariosListNew;
            tarjetas.setUsuariosList(usuariosListNew);
            tarjetas = em.merge(tarjetas);
            for (Usuarios usuariosListOldUsuarios : usuariosListOld) {
                if (!usuariosListNew.contains(usuariosListOldUsuarios)) {
                    usuariosListOldUsuarios.setIdTarjeta(null);
                    usuariosListOldUsuarios = em.merge(usuariosListOldUsuarios);
                }
            }
            for (Usuarios usuariosListNewUsuarios : usuariosListNew) {
                if (!usuariosListOld.contains(usuariosListNewUsuarios)) {
                    Tarjetas oldIdTarjetaOfUsuariosListNewUsuarios = usuariosListNewUsuarios.getIdTarjeta();
                    usuariosListNewUsuarios.setIdTarjeta(tarjetas);
                    usuariosListNewUsuarios = em.merge(usuariosListNewUsuarios);
                    if (oldIdTarjetaOfUsuariosListNewUsuarios != null && !oldIdTarjetaOfUsuariosListNewUsuarios.equals(tarjetas)) {
                        oldIdTarjetaOfUsuariosListNewUsuarios.getUsuariosList().remove(usuariosListNewUsuarios);
                        oldIdTarjetaOfUsuariosListNewUsuarios = em.merge(oldIdTarjetaOfUsuariosListNewUsuarios);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tarjetas.getIdTarjeta();
                if (findTarjetas(id) == null) {
                    throw new NonexistentEntityException("The tarjetas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Tarjetas tarjetas;
            try {
                tarjetas = em.getReference(Tarjetas.class, id);
                tarjetas.getIdTarjeta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tarjetas with id " + id + " no longer exists.", enfe);
            }
            List<Usuarios> usuariosList = tarjetas.getUsuariosList();
            for (Usuarios usuariosListUsuarios : usuariosList) {
                usuariosListUsuarios.setIdTarjeta(null);
                usuariosListUsuarios = em.merge(usuariosListUsuarios);
            }
            em.remove(tarjetas);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } 
    }

    public List<Tarjetas> findTarjetasEntities() {
        return findTarjetasEntities(true, -1, -1);
    }

    public List<Tarjetas> findTarjetasEntities(int maxResults, int firstResult) {
        return findTarjetasEntities(false, maxResults, firstResult);
    }

    private List<Tarjetas> findTarjetasEntities(boolean all, int maxResults, int firstResult) {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tarjetas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
    }
    
    public Object findCardByNumber(String no_tarjeta){
        return em.createNamedQuery("Tarjetas.findByNoTarjeta").setParameter("noTarjeta", no_tarjeta);
    }

    public Tarjetas findTarjetas(Integer id) {
        
            return em.find(Tarjetas.class, id);
        
    }

    public int getTarjetasCount() {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tarjetas> rt = cq.from(Tarjetas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        
    }

}
