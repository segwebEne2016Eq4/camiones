/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import session.CamionViajeFacade;
import entity.*;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
//import javax.faces.bean.RequestScoped;
//import javax.faces.bean.ViewScoped;

/**
 *
 * @author ASUS
 */
@ManagedBean
@SessionScoped
public class ComprasController implements Serializable {

    @EJB
    private CamionViajeFacade camionViajeFacade;
    private CamionViaje camionViajeSeleccionado;
    private String origen;
    private String destino;
    private int cantidadBoletos;
    private Date fecha;
    private List<String> listaDestinos = new ArrayList<>();
    private List<CamionViaje> listaViajes = new ArrayList<>();
    private boolean btnContinuar;

    @ManagedProperty(value = "#{loginBean}")
    LoginBean objetoBean;
//    

    public ComprasController() {
        cantidadBoletos = 1;
        btnContinuar = true;
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            //ExternalContext externalContext = context.getExternalContext();
            Object usuario = context.getExternalContext().getSessionMap().get("loginBean");
            objetoBean = null;
            if (usuario != null) {
                objetoBean = (LoginBean) usuario;
            }
        } catch (Exception e) {
            System.out.println("Se aloco esa cosa" + e.getMessage());
        }

    }

    public void cargarDestinos() {
        try {
            System.out.println("userrrrrrrrrrrrrrrr " + objetoBean.user.getUsername());

        } catch (NullPointerException e) {
            System.out.println("vaciooooo como tu corazon marin :D");
        }
        this.listaDestinos = this.camionViajeFacade.findDestinos(origen);
    }

    public void cargarViajes() {
        listaViajes = this.camionViajeFacade.findFiltro(origen, destino, cantidadBoletos, fecha);
    }

    public float totalVenta() {

        return (cantidadBoletos * camionViajeSeleccionado.getIdViaje().getIdDestino().getTarifa());
    }

    public String tipoAutobus(int tipo) {
        String tipoA = "";
        switch (tipo) {
            case 1:
                tipoA = "Normal";
                break;
            case 2:
                tipoA = "Dos pisos";
                break;
        }
        return tipoA;
    }

    public boolean validarSeleccion() {
        btnContinuar = !btnContinuar;
        return btnContinuar;
    }
//    public String continuar(){
//        
//    }

    

    public boolean isBtnContinuar() {
        return btnContinuar;
    }

    public void setBtnContinuar(boolean btnContinuar) {
        this.btnContinuar = btnContinuar;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public int getCantidadBoletos() {
        return cantidadBoletos;
    }

    public void setCantidadBoletos(int cantidadBoletos) {
        this.cantidadBoletos = cantidadBoletos;
    }

    public List<String> getListaDestinos() {
        return listaDestinos;
    }

    public void setListaDestinos(ArrayList<String> listaViajes) {
        this.listaDestinos = listaViajes;
    }

    /**
     * @return the
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the listaViajes
     */
    public List<CamionViaje> getListaViajes() {
        return listaViajes;
    }

    /**
     * @param listaViajes the listaViajes to set
     */
    public void setListaViajes(List<CamionViaje> listaViajes) {
        this.listaViajes = listaViajes;
    }

    /**
     * @return the camionViajeSeleccionado
     */
    public CamionViaje getCamionViajeSeleccionado() {
        return camionViajeSeleccionado;
    }

    /**
     * @param camionViajeSeleccionado the camionViajeSeleccionado to set
     */
    public void setCamionViajeSeleccionado(CamionViaje camionViajeSeleccionado) {
        this.camionViajeSeleccionado = camionViajeSeleccionado;
    }

    public LoginBean getObjetoBean() {
        return objetoBean;
    }

    public void setObjetoBean(LoginBean objetoBean) {
        this.objetoBean = objetoBean;
    }

}
