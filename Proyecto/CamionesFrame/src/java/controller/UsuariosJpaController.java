package controller;

import control.exceptions.NonexistentEntityException;
import control.exceptions.PreexistingEntityException;
import control.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import entity.Tarjetas;
import entity.Usuarios;
import javax.persistence.NoResultException;

/**
 *
 * @author krozz
 */
@Named
@RequestScoped
public class UsuariosJpaController implements Serializable {

    @Resource
    UserTransaction utx;
    @PersistenceContext(unitName="CamionesFramePU")
    EntityManager em;

    public UsuariosJpaController() {

    }

    public boolean create(Usuarios usuarios) throws PreexistingEntityException, RollbackFailureException, Exception {
        boolean ret;
        try {
            utx.begin();
            Tarjetas idTarjeta = usuarios.getIdTarjeta();
            if (idTarjeta != null) {
                idTarjeta = em.getReference(idTarjeta.getClass(), idTarjeta.getIdTarjeta());
                usuarios.setIdTarjeta(idTarjeta);
            }
            em.persist(usuarios);
            if (idTarjeta != null) {
                idTarjeta.getUsuariosList().add(usuarios);
                idTarjeta = em.merge(idTarjeta);
            }
            utx.commit();
            ret = true;
        } catch (Exception ex) {
            try {
                ret = false;
                Tarjetas tempCard = usuarios.getIdTarjeta();
                utx.begin();
                em.remove(tempCard);
                utx.commit();
                utx.rollback();
            } catch (Exception re) {
                ret =false;
                
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findUsuarios(usuarios.getUsername()) != null) {
                throw new PreexistingEntityException("Usuarios " + usuarios + " already exists.", ex);
            }
            throw ex;
        } 
        return ret;
    }

    public void edit(Usuarios usuarios) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Usuarios persistentUsuarios = em.find(Usuarios.class, usuarios.getUsername());
            Tarjetas idTarjetaOld = persistentUsuarios.getIdTarjeta();
            Tarjetas idTarjetaNew = usuarios.getIdTarjeta();
            if (idTarjetaNew != null) {
                idTarjetaNew = em.getReference(idTarjetaNew.getClass(), idTarjetaNew.getIdTarjeta());
                usuarios.setIdTarjeta(idTarjetaNew);
            }
            usuarios = em.merge(usuarios);
            if (idTarjetaOld != null && !idTarjetaOld.equals(idTarjetaNew)) {
                idTarjetaOld.getUsuariosList().remove(usuarios);
                idTarjetaOld = em.merge(idTarjetaOld);
            }
            if (idTarjetaNew != null && !idTarjetaNew.equals(idTarjetaOld)) {
                idTarjetaNew.getUsuariosList().add(usuarios);
                idTarjetaNew = em.merge(idTarjetaNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usuarios.getUsername();
                if (findUsuarios(id) == null) {
                    throw new NonexistentEntityException("The usuarios with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(String id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Usuarios usuarios;
            try {
                usuarios = em.getReference(Usuarios.class, id);
                usuarios.getUsername();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuarios with id " + id + " no longer exists.", enfe);
            }
            Tarjetas idTarjeta = usuarios.getIdTarjeta();
            if (idTarjeta != null) {
                idTarjeta.getUsuariosList().remove(usuarios);
                idTarjeta = em.merge(idTarjeta);
            }
            em.remove(usuarios);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } 
    }

    public List<Usuarios> findUsuariosEntities() {
        return findUsuariosEntities(true, -1, -1);
    }

    public List<Usuarios> findUsuariosEntities(int maxResults, int firstResult) {
        return findUsuariosEntities(false, maxResults, firstResult);
    }

    private List<Usuarios> findUsuariosEntities(boolean all, int maxResults, int firstResult) {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuarios.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
    }

    public Usuarios findUsuarios(String id) {
            return em.find(Usuarios.class, id);
        
    }
    
    public Usuarios findByMail(String correo){
        /*Query q = em.createNamedQuery("SELECT u FROM Usuarios u WHERE u.correo = :correo");
        q.setParameter("correo", correo);
        
        try{
            return (Usuarios) q.getSingleResult();
        }catch(NoResultException o){
            return null;
        }*/
        try{
         Usuarios user = em.createNamedQuery("Usuarios.findByCorreo",Usuarios.class).setParameter("correo", correo).getSingleResult();
         
         if (user == null)
             return null;
         else
             return user;
        }catch(NoResultException o){
            return null;
        }
    }

    public int getUsuariosCount() {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuarios> rt = cq.from(Usuarios.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        
    }

}
