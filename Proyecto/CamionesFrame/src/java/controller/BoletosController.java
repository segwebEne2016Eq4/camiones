/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import session.BoletosFacade;

/**
 *
 * @author ASUS
 */
@ManagedBean
@SessionScoped
public class BoletosController {

    @EJB
    private BoletosFacade boletosFacade;
    private int cantidadBoletos;
    private Usuarios usuario;
    private Tarjetas tarjeta;
    private List<Boletos> listaBoletosComprados = new ArrayList<>();
    private String tiempoEspera = "1:00";
    private int minutos = 1;
    private int segundos = 59;
    private boolean terminos;

    @ManagedProperty(value = "#{loginBean}")
    LoginBean objetoBean;

    /**
     * Creates a new instance of BoletosController
     */
    public BoletosController() {
        terminos = true;
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            //ExternalContext externalContext = context.getExternalContext();
            Object usuario = context.getExternalContext().getSessionMap().get("loginBean");
            objetoBean = null;
            if (usuario != null) {
                objetoBean = (LoginBean) usuario;
            }
        } catch (Exception e) {
        }
    }

    public void temporizador() {
        if (minutos >= 0 && segundos > 0) {
            segundos--;
            tiempoEspera = minutos + ":" + segundos;
            if (segundos == 0) {
                minutos--;
                segundos = 59;
            }
        } else {
            try {
                eliminarApartados();
            } catch (IOException e) {
            }
        }
//        System.out.println("tiempo actual " + tiempoEspera);

    }

    public List<Boletos> cargarSeleccion(CamionViaje camionViaje, int cantidadBoletos) {
        minutos = 0;
        segundos = 59;
        tiempoEspera = "1:00";
        listaBoletosComprados = new ArrayList<>();
//        tarjeta = new Tarjetas();
//        usuario = new Usuarios();
//        tarjeta.setIdTarjeta(1);
//        tarjeta.setNoTarjeta("123456");
//        usuario.setUsername("Cesar");
//        usuario.setIdTarjeta(tarjeta);
        for (int i = 1; i <= cantidadBoletos; i++) {
            Boletos boleto = new Boletos();
//            boleto.setIdBoleto(i);
            boleto.setUsername(objetoBean.user);
            boleto.setIdCamionViaje(camionViaje);
            boleto.setEstatus(2);
            listaBoletosComprados.add(boleto);
        }
        return listaBoletosComprados;
    }

    public String estatusBoleto(int tipo) {
        String tipoA = "";
        switch (tipo) {
            case 1:
                tipoA = "Vendido";
                break;
            case 2:
                tipoA = "Apartado";
                break;
        }
        return tipoA;
    }

    public String apartarBoletos() {
        for (Boletos listaBoletosComprado : listaBoletosComprados) {
            this.boletosFacade.create(listaBoletosComprado);
        }
        return "venta_paso_2";
    }

    public List<Boletos> boletosComprados() {
        listaBoletosComprados = new ArrayList<>();
        List<Boletos> bols = this.boletosFacade.findAll();
        for (Boletos boleto : bols) {
            if (boleto.getEstatus() == 2 && boleto.getUsername().getUsername().equalsIgnoreCase(objetoBean.getUsername())) {
                listaBoletosComprados.add(boleto);
            }
        }
        return listaBoletosComprados;
    }

    public void eliminarApartados() throws IOException {
        listaBoletosComprados = new ArrayList<>();
        List<Boletos> bols = this.boletosFacade.findAll();
        for (Boletos boleto : bols) {
            if (boleto.getEstatus() == 2 && boleto.getUsername().getUsername().equalsIgnoreCase(objetoBean.getUsername())) {
                this.boletosFacade.remove(boleto);
            }
        }

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(externalContext.getRequestContextPath() + "/faces/index.xhtml?faces-redirect=true");

    }

    public String comprarApartados() {

        List<Boletos> bols = this.boletosFacade.findAll();
        for (Boletos boleto : bols) {
            if (boleto.getEstatus() == 2 && boleto.getUsername().getUsername().equalsIgnoreCase(objetoBean.getUsername())) {
                boleto.setEstatus(1);
                this.boletosFacade.edit(boleto);
            }
        }
        return "venta_paso_3";
    }

    public boolean terminosCondiciones() {
        terminos = !terminos;
        return terminos;
    }

    public String ultimosDigitos() {
        String numeroTarjeta = objetoBean.user.getIdTarjeta().getNoTarjeta();
        return numeroTarjeta.substring(12, 15);

    }

    public LoginBean getObjetoBean() {
        return objetoBean;
    }

    public void setObjetoBean(LoginBean objetoBean) {
        this.objetoBean = objetoBean;
    }

    public boolean isTerminos() {
        return terminos;
    }

    public void setTerminos(boolean terminos) {
        this.terminos = terminos;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public int getSegundos() {
        return segundos;
    }

    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }

    public String getTiempoEspera() {
        return tiempoEspera;
    }

    public void setTiempoEspera(String tiempoEspera) {
        this.tiempoEspera = tiempoEspera;
    }

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }

    public Tarjetas getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjetas tarjeta) {
        this.tarjeta = tarjeta;
    }

    public List<Boletos> getListaBoletosComprados() {
        return listaBoletosComprados;
    }

    public void setListaBoletosComprados(List<Boletos> listaBoletosComprados) {
        this.listaBoletosComprados = listaBoletosComprados;
    }

    public BoletosFacade getBoletosFacade() {
        return boletosFacade;
    }

    public void setBoletosFacade(BoletosFacade boletosFacade) {
        this.boletosFacade = boletosFacade;
    }

    public int getCantidadBoletos() {
        return cantidadBoletos;
    }

    public void setCantidadBoletos(int cantidadBoletos) {
        this.cantidadBoletos = cantidadBoletos;
    }
}
