/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author shayde
 */
@Entity
@Table(name = "camiones")
@NamedQueries({
    @NamedQuery(name = "Camiones.findAll", query = "SELECT c FROM Camiones c"),
    @NamedQuery(name = "Camiones.findByIdCamion", query = "SELECT c FROM Camiones c WHERE c.idCamion = :idCamion"),
    @NamedQuery(name = "Camiones.findByTipoCamion", query = "SELECT c FROM Camiones c WHERE c.tipoCamion = :tipoCamion"),
    @NamedQuery(name = "Camiones.findByCapacidad", query = "SELECT c FROM Camiones c WHERE c.capacidad = :capacidad"),
    @NamedQuery(name = "Camiones.findByEstatus", query = "SELECT c FROM Camiones c WHERE c.estatus = :estatus")})
public class Camiones implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_camion")
    private Integer idCamion;
    @Column(name = "tipo_camion")
    private Integer tipoCamion;
    @Column(name = "capacidad")
    private Integer capacidad;
    @Column(name = "estatus")
    private Integer estatus;
    @OneToMany(mappedBy = "idCamion")
    private Collection<CamionViaje> camionViajeCollection;

    public Camiones() {
    }

    public Camiones(Integer idCamion) {
        this.idCamion = idCamion;
    }

    public Integer getIdCamion() {
        return idCamion;
    }

    public void setIdCamion(Integer idCamion) {
        this.idCamion = idCamion;
    }

    public Integer getTipoCamion() {
        return tipoCamion;
    }

    public void setTipoCamion(Integer tipoCamion) {
        this.tipoCamion = tipoCamion;
    }

    public Integer getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Integer capacidad) {
        this.capacidad = capacidad;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public Collection<CamionViaje> getCamionViajeCollection() {
        return camionViajeCollection;
    }

    public void setCamionViajeCollection(Collection<CamionViaje> camionViajeCollection) {
        this.camionViajeCollection = camionViajeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCamion != null ? idCamion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Camiones)) {
            return false;
        }
        Camiones other = (Camiones) object;
        if ((this.idCamion == null && other.idCamion != null) || (this.idCamion != null && !this.idCamion.equals(other.idCamion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Camiones[ idCamion=" + idCamion + " ]";
    }
    
}
