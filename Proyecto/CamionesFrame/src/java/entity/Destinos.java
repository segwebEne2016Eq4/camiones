/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author shayde
 */
@Entity
@Table(name = "destinos")
@NamedQueries({
    @NamedQuery(name = "Destinos.findAll", query = "SELECT d FROM Destinos d"),
    @NamedQuery(name = "Destinos.findByIdDestino", query = "SELECT d FROM Destinos d WHERE d.idDestino = :idDestino"),
    @NamedQuery(name = "Destinos.findByNombreOrigen", query = "SELECT d FROM Destinos d WHERE d.nombreOrigen = :nombreOrigen"),
    @NamedQuery(name = "Destinos.findByNombreDestino", query = "SELECT d FROM Destinos d WHERE d.nombreDestino = :nombreDestino"),
    @NamedQuery(name = "Destinos.findByTiempoRecorrido", query = "SELECT d FROM Destinos d WHERE d.tiempoRecorrido = :tiempoRecorrido"),
    @NamedQuery(name = "Destinos.findByTarifa", query = "SELECT d FROM Destinos d WHERE d.tarifa = :tarifa")})
public class Destinos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_destino")
    private Integer idDestino;
    @Size(max = 45)
    @Column(name = "nombre_origen")
    private String nombreOrigen;
    @Size(max = 45)
    @Column(name = "nombre_destino")
    private String nombreDestino;
    @Size(max = 45)
    @Column(name = "tiempo_recorrido")
    private String tiempoRecorrido;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "tarifa")
    private Float tarifa;
    @OneToMany(mappedBy = "idDestino")
    private Collection<Viajes> viajesCollection;

    public Destinos() {
    }

    public Destinos(Integer idDestino) {
        this.idDestino = idDestino;
    }

    public Integer getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(Integer idDestino) {
        this.idDestino = idDestino;
    }

    public String getNombreOrigen() {
        return nombreOrigen;
    }

    public void setNombreOrigen(String nombreOrigen) {
        this.nombreOrigen = nombreOrigen;
    }

    public String getNombreDestino() {
        return nombreDestino;
    }

    public void setNombreDestino(String nombreDestino) {
        this.nombreDestino = nombreDestino;
    }

    public String getTiempoRecorrido() {
        return tiempoRecorrido;
    }

    public void setTiempoRecorrido(String tiempoRecorrido) {
        this.tiempoRecorrido = tiempoRecorrido;
    }

    public Float getTarifa() {
        return tarifa;
    }

    public void setTarifa(Float tarifa) {
        this.tarifa = tarifa;
    }

    public Collection<Viajes> getViajesCollection() {
        return viajesCollection;
    }

    public void setViajesCollection(Collection<Viajes> viajesCollection) {
        this.viajesCollection = viajesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDestino != null ? idDestino.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Destinos)) {
            return false;
        }
        Destinos other = (Destinos) object;
        if ((this.idDestino == null && other.idDestino != null) || (this.idDestino != null && !this.idDestino.equals(other.idDestino))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Destinos[ idDestino=" + idDestino + " ]";
    }
    
}
