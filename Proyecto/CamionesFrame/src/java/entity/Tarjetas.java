package entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author krozz
 */
@Entity
@Table(name = "tarjetas")
@NamedQueries({
    @NamedQuery(name = "Tarjetas.findAll", query = "SELECT t FROM Tarjetas t"),
    @NamedQuery(name = "Tarjetas.findByIdTarjeta", query = "SELECT t FROM Tarjetas t WHERE t.idTarjeta = :idTarjeta"),
    @NamedQuery(name = "Tarjetas.findByNoTarjeta", query = "SELECT t FROM Tarjetas t WHERE t.noTarjeta = :noTarjeta"),
    @NamedQuery(name = "Tarjetas.findByFechaCad", query = "SELECT t FROM Tarjetas t WHERE t.fechaCad = :fechaCad"),
    @NamedQuery(name = "Tarjetas.findByCodigoSeg", query = "SELECT t FROM Tarjetas t WHERE t.codigoSeg = :codigoSeg")})
public class Tarjetas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tarjeta")
    private Integer idTarjeta;
    @Size(max = 16)
    @Column(name = "no_tarjeta")
    private String noTarjeta;
    @Column(name = "fecha_cad")
    @Temporal(TemporalType.DATE)
    private Date fechaCad;
    @Column(name = "codigo_seg")
    private Integer codigoSeg;
    @OneToMany(mappedBy = "idTarjeta")
    private List<Usuarios> usuariosList;

    public Tarjetas() {
    }

    public Tarjetas(Integer idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public Integer getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdTarjeta(Integer idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public String getNoTarjeta() {
        return noTarjeta;
    }

    public void setNoTarjeta(String noTarjeta) {
        this.noTarjeta = noTarjeta;
    }

    public Date getFechaCad() {
        return fechaCad;
    }

    public void setFechaCad(Date fechaCad) {
        this.fechaCad = fechaCad;
    }

    public Integer getCodigoSeg() {
        return codigoSeg;
    }

    public void setCodigoSeg(Integer codigoSeg) {
        this.codigoSeg = codigoSeg;
    }

    public List<Usuarios> getUsuariosList() {
        return usuariosList;
    }

    public void setUsuariosList(List<Usuarios> usuariosList) {
        this.usuariosList = usuariosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTarjeta != null ? idTarjeta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarjetas)) {
            return false;
        }
        Tarjetas other = (Tarjetas) object;
        if ((this.idTarjeta == null && other.idTarjeta != null) || (this.idTarjeta != null && !this.idTarjeta.equals(other.idTarjeta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sesion.Tarjetas[ idTarjeta=" + idTarjeta + " ]";
    }

}
