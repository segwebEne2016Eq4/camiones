/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author shayde
 */
@Entity
@Table(name = "viajes")
@NamedQueries({
    @NamedQuery(name = "Viajes.findAll", query = "SELECT v FROM Viajes v"),
    @NamedQuery(name = "Viajes.findByIdViaje", query = "SELECT v FROM Viajes v WHERE v.idViaje = :idViaje"),
    @NamedQuery(name = "Viajes.findByCupo", query = "SELECT v FROM Viajes v WHERE v.cupo = :cupo"),
    @NamedQuery(name = "Viajes.findBySalida", query = "SELECT v FROM Viajes v WHERE v.salida = :salida"),
    @NamedQuery(name = "Viajes.findByLlegada", query = "SELECT v FROM Viajes v WHERE v.llegada = :llegada"),
    @NamedQuery(name = "Viajes.findByEstatus", query = "SELECT v FROM Viajes v WHERE v.estatus = :estatus")})
public class Viajes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_viaje")
    private Integer idViaje;
    @Column(name = "cupo")
    private Integer cupo;
    @Column(name = "salida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date salida;
    @Column(name = "llegada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date llegada;
    @Column(name = "estatus")
    private Integer estatus;
    @JoinColumn(name = "id_destino", referencedColumnName = "id_destino")
    @ManyToOne
    private Destinos idDestino;
    @OneToMany(mappedBy = "idViaje")
    private Collection<CamionViaje> camionViajeCollection;

    public Viajes() {
    }

    public Viajes(Integer idViaje) {
        this.idViaje = idViaje;
    }

    public Integer getIdViaje() {
        return idViaje;
    }

    public void setIdViaje(Integer idViaje) {
        this.idViaje = idViaje;
    }

    public Integer getCupo() {
        return cupo;
    }

    public void setCupo(Integer cupo) {
        this.cupo = cupo;
    }

    public Date getSalida() {
        return salida;
    }

    public void setSalida(Date salida) {
        this.salida = salida;
    }

    public Date getLlegada() {
        return llegada;
    }

    public void setLlegada(Date llegada) {
        this.llegada = llegada;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public Destinos getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(Destinos idDestino) {
        this.idDestino = idDestino;
    }

    public Collection<CamionViaje> getCamionViajeCollection() {
        return camionViajeCollection;
    }

    public void setCamionViajeCollection(Collection<CamionViaje> camionViajeCollection) {
        this.camionViajeCollection = camionViajeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idViaje != null ? idViaje.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Viajes)) {
            return false;
        }
        Viajes other = (Viajes) object;
        if ((this.idViaje == null && other.idViaje != null) || (this.idViaje != null && !this.idViaje.equals(other.idViaje))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Viajes[ idViaje=" + idViaje + " ]";
    }
    
}
