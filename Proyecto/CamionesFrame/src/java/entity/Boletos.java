/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author shayde
 */
@Entity
@Table(name = "boletos")
@NamedQueries({
    @NamedQuery(name = "Boletos.findAll", query = "SELECT b FROM Boletos b"),
    @NamedQuery(name = "Boletos.findByIdBoleto", query = "SELECT b FROM Boletos b WHERE b.idBoleto = :idBoleto"),
    @NamedQuery(name = "Boletos.findByFechaCompra", query = "SELECT b FROM Boletos b WHERE b.fechaCompra = :fechaCompra"),
    @NamedQuery(name = "Boletos.findByEstatus", query = "SELECT b FROM Boletos b WHERE b.estatus = :estatus")})
public class Boletos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_boleto")
    private Integer idBoleto;
    @Column(name = "fecha_compra")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCompra;
    @Column(name = "estatus")
    private Integer estatus;
    @JoinColumn(name = "id_camion_viaje", referencedColumnName = "id_camion_viaje")
    @ManyToOne
    private CamionViaje idCamionViaje;
    @JoinColumn(name = "username", referencedColumnName = "username")
    @ManyToOne
    private Usuarios username;

    public Boletos() {
    }

    public Boletos(Integer idBoleto) {
        this.idBoleto = idBoleto;
    }

    public Integer getIdBoleto() {
        return idBoleto;
    }

    public void setIdBoleto(Integer idBoleto) {
        this.idBoleto = idBoleto;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public CamionViaje getIdCamionViaje() {
        return idCamionViaje;
    }

    public void setIdCamionViaje(CamionViaje idCamionViaje) {
        this.idCamionViaje = idCamionViaje;
    }

    public Usuarios getUsername() {
        return username;
    }

    public void setUsername(Usuarios username) {
        this.username = username;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBoleto != null ? idBoleto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Boletos)) {
            return false;
        }
        Boletos other = (Boletos) object;
        if ((this.idBoleto == null && other.idBoleto != null) || (this.idBoleto != null && !this.idBoleto.equals(other.idBoleto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Boletos[ idBoleto=" + idBoleto + " ]";
    }
    
}
