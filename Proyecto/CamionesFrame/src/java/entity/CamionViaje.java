/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author shayde
 */
@Entity
@Table(name = "camion_viaje")
@NamedQueries({
    @NamedQuery(name = "CamionViaje.findAll", query = "SELECT c FROM CamionViaje c"),
    @NamedQuery(name = "CamionViaje.findByIdCamionViaje", query = "SELECT c FROM CamionViaje c WHERE c.idCamionViaje = :idCamionViaje")})
public class CamionViaje implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_camion_viaje")
    private Integer idCamionViaje;
    @OneToMany(mappedBy = "idCamionViaje")
    private Collection<Boletos> boletosCollection;
    @JoinColumn(name = "id_viaje", referencedColumnName = "id_viaje")
    @ManyToOne
    private Viajes idViaje;
    @JoinColumn(name = "id_camion", referencedColumnName = "id_camion")
    @ManyToOne
    private Camiones idCamion;

    public CamionViaje() {
    }

    public CamionViaje(Integer idCamionViaje) {
        this.idCamionViaje = idCamionViaje;
    }

    public Integer getIdCamionViaje() {
        return idCamionViaje;
    }

    public void setIdCamionViaje(Integer idCamionViaje) {
        this.idCamionViaje = idCamionViaje;
    }

    public Collection<Boletos> getBoletosCollection() {
        return boletosCollection;
    }

    public void setBoletosCollection(Collection<Boletos> boletosCollection) {
        this.boletosCollection = boletosCollection;
    }

    public Viajes getIdViaje() {
        return idViaje;
    }

    public void setIdViaje(Viajes idViaje) {
        this.idViaje = idViaje;
    }

    public Camiones getIdCamion() {
        return idCamion;
    }

    public void setIdCamion(Camiones idCamion) {
        this.idCamion = idCamion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCamionViaje != null ? idCamionViaje.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CamionViaje)) {
            return false;
        }
        CamionViaje other = (CamionViaje) object;
        if ((this.idCamionViaje == null && other.idCamionViaje != null) || (this.idCamionViaje != null && !this.idCamionViaje.equals(other.idCamionViaje))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CamionViaje[ idCamionViaje=" + idCamionViaje + " ]";
    }
    
}
