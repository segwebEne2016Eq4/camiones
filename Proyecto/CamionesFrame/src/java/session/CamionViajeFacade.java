/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package session;

import entity.CamionViaje;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author shayde
 */
@Stateless
public class CamionViajeFacade extends AbstractFacade<CamionViaje> {
    @PersistenceContext(unitName = "CamionesFramePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CamionViajeFacade() {
        super(CamionViaje.class);
    }
    public List<CamionViaje> findFiltro(String origen, String destino, int cantidad, Date fecha){
       List<CamionViaje> viajes= this.findAll();
       List<CamionViaje> viajesFiltrados =new ArrayList<>();
       for(CamionViaje viaje:viajes){
           Date fechaViaje= viaje.getIdViaje().getSalida();
           if(origen.equalsIgnoreCase(viaje.getIdViaje().getIdDestino().getNombreOrigen())){
               if(destino.equalsIgnoreCase(viaje.getIdViaje().getIdDestino().getNombreDestino())){
                   if(fecha.getDay()==fechaViaje.getDay() && fecha.getMonth()==fechaViaje.getMonth() && fecha.getYear()==fechaViaje.getYear()){
                       if(cantidad<= viaje.getIdViaje().getCupo()){
                           
                         viajesFiltrados.add(viaje);
                       }
                   }
               }
           }
       }
        return viajesFiltrados;
    }
    public List<String> findDestinos(String origen){
        List<CamionViaje> viajes= this.findAll();
       List<String> destinosFiltrados =new ArrayList<>();
       for(CamionViaje viaje:viajes){
           if(origen.equalsIgnoreCase(viaje.getIdViaje().getIdDestino().getNombreOrigen())){
               if(!destinosFiltrados.contains(viaje.getIdViaje().getIdDestino().getNombreDestino())){
                   destinosFiltrados.add(viaje.getIdViaje().getIdDestino().getNombreDestino());
               }
               
           }
       }
       return destinosFiltrados;
    }
    
}
