/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Boletos;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ASUS
 */

@Stateless
public class BoletosFacade extends AbstractFacade<Boletos> {

    
    @PersistenceContext(unitName = "CamionesFramePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BoletosFacade() {
        super(Boletos.class);
    }
//    public void agregar(List<Boletos> lista){
//        for (Boletos boletos : lista) {
//            create(boletos);
//            
//        }
//        
//    }
    
}
