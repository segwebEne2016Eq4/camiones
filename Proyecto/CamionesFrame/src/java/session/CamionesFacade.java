/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package session;

import entity.Camiones;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author shayde
 */
@Stateless
public class CamionesFacade extends AbstractFacade<Camiones> {
    @PersistenceContext(unitName = "CamionesFramePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CamionesFacade() {
        super(Camiones.class);
    }
    
}
