package utils;



import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;

/**
 *
 * @author krozz
 */
public class encriptador {

    public String encripta(String p) {
        String llave = null;
        MessageDigest digest;
        byte[] hash;
        int l = 0;
        StringBuffer hexString = new StringBuffer();
        try {
            digest = MessageDigest.getInstance("SHA-256");
            try {
                hash = digest.digest(p.getBytes("UTF-8"));
                l = hash.length;
                for (int i = 0; i < l; i++) {
                    String hex = Integer.toHexString(0xff & hash[i]);
                    if (hex.length() == 1) {
                        hexString.append('0');
                    }
                    hexString.append(hex);
                }
            } catch (UnsupportedEncodingException es) {
            }
            llave = hexString.toString();

        } catch (NoSuchAlgorithmException ex) {
        }
        return llave;
    }

   

    public static void main(String[] args) {
        encriptador e = new encriptador();

        System.out.println("hashed 256 user4: " + e.encripta("pass6"));
        System.out.println("hashed 256 ventas: " + e.encripta("ventas"));
    }
}
